

// Array of products
const Products = [
  {
    title: '1984',
    author: 'George Orwell',
    rating: '5',
    categories: ['Dystopian', 'Fiction', 'Classic']
  },

  {
    title: 'Hamlet',
    author: 'William Shakespeare (Shakespeare or not Shakespeare that is the question!)',
    rating: '5',
    categories: ['Classic']
  },

  {
    title: 'The Glass Bead Game',
    author: 'Hermann Hesse',
    rating: '5+',
    categories: ['Fiction', 'Fantasy', 'Fictional Biography']
  },

  {
    title: 'The Immortal',
    author: 'Jorge Luis Borges',
    rating: 'BEYOUND IMAGINATION',
    categories: ['Fiction', 'Short Story']
  },

  {
    title: 'The Time Machine',
    author: 'H. G. Wells',
    rating: '5',
    categories: ['Fiction', 'Science Fiction']
  },

  {
    title: 'Brave New World',
    author: 'Aldous Huxley',
    rating: '4',
    categories: ['Dystopian', 'Fiction', 'Science Fiction']
  }
]



// Object of products
const ProductsObj = {
  1: {
    title: '1984',
    author: 'George Orwell',
    rating: '5',
    categories: ['Dystopian', 'Fiction', 'Classic']
  },

  2: {
    title: 'Hamlet',
    author: 'William Shakespeare (Shakespeare or not Shakespeare that is the question!)',
    rating: '5',
    categories: ['Classic']
  },

  3: {
    title: 'The Glass Bead Game',
    author: 'Hermann Hesse',
    rating: '5+',
    categories: ['Fiction', 'Fantasy', 'Fictional Biography']
  },

  4: {
    title: 'The Immortal',
    author: 'Jorge Luis Borges',
    rating: 'BEYOUND IMAGINATION',
    categories: ['Fiction', 'Short Story']
  },

  5: {
    title: 'The Time Machine',
    author: 'H. G. Wells',
    rating: '5',
    categories: ['Fiction', 'Science Fiction']
  },

  6: {
    title: 'Brave New World',
    author: 'Aldous Huxley',
    rating: '4',
    categories: ['Dystopian', 'Fiction', 'Science Fiction']
  }
}




/**
 * filter by categories
 * description: prototype of Object or Array
 * @param {arguments || Array of arguments} categories filter fields
 * @return {Array} filtered items
 */

Object.prototype.with_categories = function( ...argums ) {

  const filter_by = 'categories'      // filter field

  const that = typeof this === 'object' ? Object.values( this ) : this                // Object or Array
  const argArr = typeof [...argums][0] === 'object' ? [...argums][0] : [...argums]    // transform arguments to Array

  const args = argArr.map( (str) => str.toLowerCase() ),    // arguments to lower case
        filtered = []                                       // result


  // get Arrays of categories
  that.map(( items, i ) => {

    // filter core
    let tmp = items[filter_by].filter(( el ) => {
      return args.indexOf( el.toLowerCase() ) !== -1
    })

    if(tmp.length === args.length) filtered.push( items )   // or filtered.push( i ) for return Array of indexes
  })

  return filtered
}



// result
console.log('out: ', Products.with_categories   (   'Dystopian', 'Fiction'    ))    // filter with simple arguments
console.log('out: ', ProductsObj.with_categories(  ['Fiction', 'Dystopian']   ))    // filter with Array arguments